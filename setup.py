import io

from setuptools import find_packages
from setuptools import setup

with io.open("README.md", "rt", encoding="utf8") as f:
    readme = f.read()

setup(
    name="talkbylot",
    version="1.0.0",
    url="https://gitlab.com/pwgd/talk-by-lot",
    license="AGPL-3",
    maintainer="People Who Give a Damn",
    description="Engine for sharing the power to talk about topics.",
    long_description=readme,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=["flask"],
    extras_require={"test": ["pytest", "coverage"]},
)
