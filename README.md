# Talk by Lot


Given x number of identifiers of people in a group,
Return y of those identifiers required to provide a reasonably scientific sampling of the people in the group.


Talk by Lot operating as a pure technical service is a bit unlikely (no one has asked for it) so we will take some simplifying approaches:

  * The identifier for a juror is truly global.  We are not taking anonymous numeric IDs and returning them back, because we are tracking a person's participation in one universe.  This saves us from creating and passing around a 'universe id' to go with each population and juror.
  * While we only store identifiers, we will keep track of all participation and decisions locally, rather than deciding on a format and sending that back to (as yet non-existent) external consumers of our service.

## Initial set-up

```
python3 -m venv venv
python -m pip install -r requirements.txt
```

## Regular use

```
source venv/bin/activate
export FLASK_APP=talkbylot.py
export FLASK_ENV=development
flask run
```

## Tests

Currently the one unit test can be run like this:

```
python -m pip install -r test_requirements.txt
cd talkbylot/
python -m unittest roles_test.py
```

And pytest tests can be run like this:

```
pip install '.[test]'
pytest
```


## Essential references

  * https://note.nkmk.me/en/python-random-choice-sample-choices/
  * The entire web of Python documentation and blog posts and Q&A as i relearn some basics.
  * [Sample Size Calculator - Confidence Level, Confidence Interval, Sample Size, Population Size, Relevant Population - Creative Research Systems](https://surveysystem.com/sscalc.htm) until i build in my own rough scientific size calculator.
  * https://docs.python.org/3/ including https://docs.python.org/3/library/unittest.html

## Notes

A speed optimization we're putting aside for now as probably not applicable (our population is likely to have changed from one drawing of lots to the next, but maybe a modified version of this approach could still be used):

  * https://www.freecodecamp.org/news/how-to-get-embarrassingly-fast-random-subset-sampling-with-python-da9b27d494d9/
