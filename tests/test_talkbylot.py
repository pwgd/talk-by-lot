import pytest

from talkbylot.db import get_db


def test_intro_page(client):
    response = client.get('/')
    assert b'Engine for sharing the power to talk about topics.' in response.data
