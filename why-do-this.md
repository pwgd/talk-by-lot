There is so much that needs to be done.  The planetary environment itself needs saving from human-caused global warming, institutions that act in our name kill people with impunity on the other side of the world and across town, and odds are a person living within a few miles of you lacks suitable shelter.  At the level that matters most, all the harm that is done, and all the good that is done, is done by people acting within systems.  Every corporation, government, or charity is itself organized people.

Many people have given up hope that things can be better.  Many people see both the need to do good and the need to organize differently.  Some may be the same people!  Access to the embedded work of other people—capital, in economic parlance—may often be needed for key changes.  But some changes can be made by organized people.  And in any case, the first step is knowing what people want, are willing to do, are able to do, and to try to figure out what is needed from people to make the changes




***

Giant chicken (multicolored metal rooster) in the small cemetary off to the left on 316 North, in Goodhue County shortly before entering Dakota County.

Seen 2020 October 18, 6:25pm.


***

CEAP - billboard for some kind of charity asking for donations and volunteering, promising sahring.
