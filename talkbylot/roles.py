import logging
import random

"""
Our goal with assigning roles is twofold:

  1.  Each person only has one role at any given time.
  2.  Each person performs each role and their share of pass before getting a role again.

And the goal with doing this by a computer program is to never think about
either of these again.
"""

def roles(population, roles=["facilitator", "notetaker"], participation={}):
  """
  Return a person from the given population to take on each of the given roles.

  We also return an updated participation history.

  This function is written to be data source agnostic.  The group population
  and the participation history get handed to it.  In practice a helper
  function 'get_roles' will be needed to wrap it and do the database work.

  :param population: all people in a group
  :type population: list
  :param roles: labels for roles people need to take on
  :type roles: list
  :param participation: a dictionary of dictionaries for each role with all the people who have performed that role already, with the person's identifier as the key and the number of times they have served that role as the value.
  :type participation: dict
  :return: A dictionary containing a dictionary for each role giving people filling that role and the .
  :rtype: dict
  """
  if len(population) < len(roles):
      # TODO a proper exception... also TODO fix indentation on this file.
      print("This script ensures no person has two roles at once, therefore "
            "it cannot work if the provided population has fewer people in it "
            "than the number of requested roles!")
      raise Exception
      # break / exit / quit / escape / whatever Python lets us do.

  result = {
      'roles': {},
      'participation': participation
  }
  roled = []

  for role in roles:
      # Add any never-before-recorded roles for safe iterating below.
      [participation.setdefault(role, {}) for role in roles if role not in participation]
      # A participation dict looks like this (two roles each with two people):
      # {'janitor': {'Tom': 2, 'Fran': 4}, 'bookkeeper': {'Jane': 3, 'Sam': 1}}
      # We don't care about the second number, keeping track of how many times
      # a person has served, right now, so we extract the people who have
      # served at all in a given role as a simple list.
      # NOTE: We do not add to/allow a person in this dict with a zero count.
      served = [i for i in participation[role].keys()]
      # Then we remove those who have served from our overall population to get
      # our pool of people eligible to serve now.
      # Also remove anyone who is PRESENTLY serving one of the requested roles.
      pool = [i for i in population if i not in served and i not in roled]
      # Note this does not throw an error if we try to remove a person who is
      # not in the population; this is intentional and cleaning up the
      # participation history to remove people no longer in the group is a
      # separate, optional task.
      expanded = 0
      while not pool:
          # TODO change to if pool <= roled because if all that's left in the
          # pool is people who already have roles this next step will leave us
          # with nothing, and because we know everyone with a role is in the...
          # ah, population, not the pool, so we can't try to head it off, we
          # have to wait and see (could be only one person left in the pool but
          # the person who already got a role in this round might have been one
          # who was removed due to participation in another round.
          # TODO sort to get the people who have served *least*.
          reservoir = keys_with_lowest_value(participation[role], expanded)
          population_only_pool = [i for i in reservoir if i in population]
          if not population_only_pool:
              # We have people in the participation history who are not in the
              # population; they need to be removed for us to exit this loop.
              for person in reservoir:
                  del participation[role][person]
          else:
              pool = [i for i in population_only_pool if not i in roled]
              if not pool:
                  expanded = len(roled)
      person = random.choice(pool)
      result['roles'][role] = person
      roled.append(person)
      try:
          count = participation[role][person]
      except KeyError:
          count = 0
      count = count + 1
      participation[role][person] = count

  result['participation'] = participation
  return result


def keys_with_lowest_value(dictionary, expanded=0):
    # This could be made more efficient.  We want to check quickly "is there
    # any key with a value of 1?" and immediately return when we find one, and
    # if not, then try 2, then 3, and so on.  But we can bail as soon as we
    # find a low integer, without sorting everything; also not known by me if
    # getting a unique set before sorting is faster than not.
    unique = sorted(set(dictionary.values()))
    lowest = unique[0]
    if lowest == 0:
        logger = logging.getLogger(__name__)
        logger.error('Zero is not an allowed value in the participation dictionary, something screwed up the data handed into this script.')
    keys = list(filter(lambda x, d=dictionary, val=lowest: d[x] == val, dictionary))
    # We know, if we have been handed an integer for expanded, that we need at
    # least that many items handed back.
    level = 1
    while expanded and len(keys) < expanded:
        lowest = unique[level]
        keys + list(filter(lambda x, d=dictionary, val=lowest: d[x] == val, dictionary))
        level += 1
    return keys
