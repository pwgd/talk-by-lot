import os
from .roles import roles
from .info import info_population_list

from flask import Flask, request


def create_app(test_config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'talkbylot.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/')
    def index():
        return '''
            <h1>Talk by Lot</h1>
            <p><em>Engine for sharing the power to talk about topics.</em></p>
            <p>Given x number of identifiers of people in a group, Return y of those identifiers required to provide a reasonably scientific sampling of the people in the group.</p>
            <p><strong><em>Under development.</em></strong></p>
            <p>Right now a <a href="/roles">demo of a bonus feature, filling an arbitrary set of roles for a team with one non-overlapping person for each, is available</a>.</p>
            <p>Source code: <a href="https://gitlab.com/pwgd/talk-by-lot">https://gitlab.com/pwgd/talk-by-lot</a></p>
        '''
    @app.route('/roles', methods=['GET', 'POST'])
    def roles_form():
        if request.method == 'POST':
            html = "<h1>Roles</h1>"
            html += "<p>Here are the random assignments of people from the Agaric team ('population') into the requested roles.</p>"
            roles_list = request.form['roles'].splitlines()
            population = ['wolcen', 'freescholar', 'dinarcon', 'keegan', 'mlncn']
            result = roles(population, roles_list)
            for role, person in result['roles'].items():
                html += "<p>" + role + ": <strong>" + person + "</strong></p>"
            return html
        return '''
            <h1>Roles</h1>
            <p><em>This is a demonstration page of the <a href="/">Talk By Lot</a> random Roles assignment library, and it is hard-coded to the Agaric worker-owner 'population'.  Other purposes should use the Talk By Lot API.</em></p>
            <p>Enter the roles you need today, one per line:</p>
            <form method="post">
                <textarea name="roles" cols="40" rows="8"></textarea><br />
                <input type="submit" value="Get role assignments">
            </form>
        '''
    
    @app.route('/populations', methods=['GET'])
    def population_list():
        html = "<h1>Populations</h1>"
        result = info_population_list()
        for population, people in result.items():
            html += "<h3>" + population + "</h3>"
            for person in people.items():
                html += person
        return html

    @app.route('/api/')
    def get_api():
        raise InvalidUsage('API autodiscovery not implemented', status_code=410)

    from talkbylot import db
    db.init_app(app)

    # from . import talkbylot
    # talkbylot.init_app(app)

    return app

app = create_app()



from flask import jsonify

class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# from talkbylot.database import db_session

# @app.teardown_appcontext
# def shutdown_session(exception=None):
#     db_session.remove()
