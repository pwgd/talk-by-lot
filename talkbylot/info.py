
"""
"""

def info_population_list():
  """
  Return populations and the people in them.

  :return: A dictionary containing a dictionary for each role giving people filling that role and the .
  :rtype: dict
  """
  result = {
      'roles': {},
      'participation': participation
  }
  roled = []

  for role in roles:
      # Add any never-before-recorded roles for safe iterating below.
      [participation.setdefault(role, {}) for role in roles if role not in participation]
      # A participation dict looks like this (two roles each with two people):
      # {'janitor': {'Tom': 2, 'Fran': 4}, 'bookkeeper': {'Jane': 3, 'Sam': 1}}
      # We don't care about the second number, keeping track of how many times
      # a person has served, right now, so we extract the people who have
      # served at all in a given role as a simple list.
      # NOTE: We do not add to/allow a person in this dict with a zero count.
      served = [i for i in participation[role].keys()]
      # Then we remove those who have served from our overall population to get
      # our pool of people eligible to serve now.
      # Also remove anyone who is PRESENTLY serving one of the requested roles.
      pool = [i for i in population if i not in served and i not in roled]
      # Note this does not throw an error if we try to remove a person who is
      # not in the population; this is intentional and cleaning up the
      # participation history to remove people no longer in the group is a
      # separate, optional task.
      expanded = 0
      while not pool:
          # TODO change to if pool <= roled because if all that's left in the
          # pool is people who already have roles this next step will leave us
          # with nothing, and because we know everyone with a role is in the...
          # ah, population, not the pool, so we can't try to head it off, we
          # have to wait and see (could be only one person left in the pool but
          # the person who already got a role in this round might have been one
          # who was removed due to participation in another round.
          # TODO sort to get the people who have served *least*.
          reservoir = keys_with_lowest_value(participation[role], expanded)
          population_only_pool = [i for i in reservoir if i in population]
          if not population_only_pool:
              # We have people in the participation history who are not in the
              # population; they need to be removed for us to exit this loop.
              for person in reservoir:
                  del participation[role][person]
          else:
              pool = [i for i in population_only_pool if not i in roled]
              if not pool:
                  expanded = len(roled)
      person = random.choice(pool)
      result['roles'][role] = person
      roled.append(person)
      try:
          count = participation[role][person]
      except KeyError:
          count = 0
      count = count + 1
      participation[role][person] = count

  result['participation'] = participation
  return result
