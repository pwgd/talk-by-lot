import roles
import unittest

class TestRoleFunction(unittest.TestCase):

    def test_no_participation_history(self):
        population = ['Jane', 'Betty', 'Tommy', 'Cedrick', 'Fran', 'Sean', 'Sally', 'Max']
        for x in range(0, len(population) * 2):
            result = roles.roles(population)
            self.assertIn(result['roles']['facilitator'], population)
            self.assertIn(result['roles']['notetaker'], population)
            self.assertNotEqual(result['roles']['facilitator'], result['roles']['notetaker'])

if __name__ == '__main__':
    unittest.main()
