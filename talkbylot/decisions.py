import click
import random
from flasgger import Swagger, swag_from
from flask_restful import Api, Resource
from flask import Blueprint
from flask.cli import with_appcontext
from talkbylot.db import get_db

DEFAULT_DUMMY_MINIMUM_SAMPLE = 330

bp = Blueprint("decisions", __name__)
api = Api(bp)


class Population(Resource):
    def get(self, population_id):
        """
        Return basic information about a population.
        ---
        tags:
          - restful
        parameters:
          - in: path
            name: population_id
            required: true
            description: The identifier for a population.
            type: string
        responses:
          200:
            description: Population information
            schema:
              id: Population
              properties:
                population:
                  type: object
        """



def sample(population=[]):
    """
    Return a scientific sample from a population.

    :param population: all people in a group
    :type population: list
    :return: The sample from the population
    :rtype: list
    """
    try:
        return random.sample(population, DEFAULT_DUMMY_MINIMUM_SAMPLE)
    except ValueError:
        return population

def jury(population_id):
    """
    Return a scientific sample from a population excluding people who have already put in their time

    OK we don't want just 'recent jurors' because if we can only remove 20 and not 200 we want
    the most recent 20, not to have to throw out the whole recent juror concept.  But just
    directly ranking everyone by most recent / most often participation and taking the least
    (recently) served isn't a good approach either, because anyone new joining the group after
    five years would have like five years of jury duty to work off (or at least, be pressed
    into service instantly before they can get acclimated.  Maybe we can keep this simple,
    come up with a formula for 'recent juror' (and include newly joined people in there) and
    just randomly throw out some of that pool if we have to press new/recent jurors into
    service.

    Oh, i think we can sort the population first by recency and then by how often someone has
    participated, so recency always wins but as people randomly don't participate when
    requested and others do, the odds will start to even out a little since the less-often
    participated are asked more.

    We should also probably grab a pool of alternates right away— let people click 'pass',
    remove anyone for whom we get a bounced e-mail from the request, even if we're only
    holding it open a few hours, we could swap in some people, and if it's being held open
    a couple days we could add a second wave to increase (semi) representative participation.

    And no one should be asked to respond to a different message request while an open one
    is in front of them.
    
    So too naive: 'SELECT identifier FROM person ORDER BY created ASC, total DESC LIMIT ?', (DEFAULT_DUMMY_MINIMUM_SAMPLE * 2,)

    I'm not happy with the 'double, then take a random sample' approach, but going second by
    second to build up the eligible population of non-recent jurors until we have enough is
    not reasonable, and on the other hand if there's tons of ties i'm not happy with SQL's 
    (non-random) random results either, so this sort of splits the difference.
    """

    db = get_db()
    jury_pool = db.execute(
        'SELECT person FROM person_population LEFT JOIN person ON person_population.person = person.identifier WHERE serving < CURRENT_TIMESTAMP ORDER BY person_population.latest ASC, person_population.total DESC, person.latest ASC, person.total DESC LIMIT ?', (DEFAULT_DUMMY_MINIMUM_SAMPLE * 2,)
    ).fetchall()

    return sample(jury_pool)

def suggest_message(message_id, population_id):
    """
    Ask a jury selected from a population to approve a message going to the full population.

    :param message_id: URI for the message.
    :type message_id: str
    :param population_id: URI for the population.
    :type population_id: str
    """
    jury = jury(population, recent_jurors)
    for juror in jury:
       request_decision(message_id, juror)

@click.command('define-population')
@click.argument('population_id')
@click.argument('population')
@with_appcontext
def define_population(population_id, population=[]):
    """
    Set the initial population.

    :param population_id: URI for the population.
    :type population_id: str

    :param population: Identifiers for all people starting out in a group.
    :type population: list
    """

    db = get_db()
    if db.execute(
        'SELECT identifier FROM population WHERE identifier = ?', (population_id,)
    ).fetchone() is not None:
        # A population with this ID already exists.
        raise ValueError("A population with this ID already exists.")
        return False

    db.execute(
        'INSERT INTO population (identifier) VALUES (?)', (population_id,)
    )
    
    # Premature optimization is the root of all evil but damn to not know how
    # to do a bulk insert because i'm working offline is sort of pathetic.
    for person_id in population:
        db.execute(
            'INSERT INTO person (identifier) VALUES (?)', (person_id,)
        )
        db.execute(
            'INSERT INTO person_population (person, population) VALUES (?, ?)',
            (person_id, population_id)
        )
    db.commit()

@click.command('add-to-population')
@click.argument('population_id')
@click.argument('people')
@with_appcontext
def add_to_population(population_id, people=[]):
    """
    Add people to a population.

    :param population_id: URI for the population.
    :type population_id: str

    :param people: Identifiers for all people to add.
    :type people: list
    """

    db = get_db()
    if db.execute(
        'SELECT identifier FROM population WHERE identifier = ?', (population_id,)
    ).fetchone() is None:
        raise ValueError("A population with this ID does not exist.")
        return False

    # Premature optimization is the root of all evil but damn to not know how
    # to do a bulk insert because i'm working offline is sort of pathetic.
    for person_id in people:
        db.execute(
            'INSERT INTO person (identifier) VALUES (?)', (person_id,)
        )
        db.execute(
            'INSERT INTO person_population (person, population) VALUES (?, ?)',
            (person_id, population_id)
        )
    db.commit()

def request_decision(message_id, juror):
    """
    Send a request to make a decision about a message to a juror.
    """
    print("Pass off an asyncronous request for " + message_id + " to be looked at by " + juror)

def receive_decision(message_id, person_id, population_id):
    """
    Update juror participation record and increment quorum for decision.
    """
    db.execute(
        'INSERT INTO participation (person, population) VALUES (?, ?)',
        (person_id, population_id)
    )
    db.execute(
        'UPDATE person'
        ' SET latest = CURRENT_TIMESTAMP, total = total + 1'
        ' WHERE person = ?', (person_id,)
    )
    db.execute(
        'UPDATE person_population'
         ' SET latest = CURRENT_TIMESTAMP, total = total + 1'
         ' WHERE person = ? AND population = ?', (person_id, population_id)
    )
    db.commit()

def init_app(app):
    app.cli.add_command(define_population)
    app.cli.add_command(add_to_population)
