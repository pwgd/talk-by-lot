from faker import Faker
import talkbylot
import unittest

class TestJurorsFunction(unittest.TestCase):

    def test_no_participation_history(self):
        fake = Faker()
        # This will frequently come out to something less than 500 because of
        # the uniqueness filter set applies.
        population = set([fake.profile(fields='mail').popitem()[1] for i in range(500)])

        for x in range(len(population)):
            jurors = talkbylot.sample(population)
            self.assertTrue(len(jurors) > 300)
            self.assertEqual(len(jurors), len(set(jurors)),
                    msg = "Our jury pool is not made up of unique jurors!")

            for j in range(len(jurors)):
                self.assertIn(jurors[j], population)

if __name__ == '__main__':
    unittest.main()
